---
date: 2015-05-19
title: How Haskellers are seen and see themselves
author: Chris Done
---

# How Haskellers are seen

## The type system and separated IO is an awkward, restricting space suit:

![](http://i.imgur.com/LKENM6i.gif)

## Spending most of their time gazing longingly at the next abstraction to yoink from mathematics:

![](http://i.imgur.com/RkwBNIP.gif)

## Looking at anything outside the Haskell language and the type system:

![](http://i.imgur.com/7kdfsf2.gif)

## Using `unsafePerformIO`:

![](http://i.imgur.com/HL5eXMj.gif)

# How Haskellers see themselves

## No, it's not a space suit. It's Iron Man's suit!

![](http://i.imgur.com/AawmZmT.gif)

## The suit enables him to do impressive feats with confidence and safety:

![](http://i.imgur.com/JU0o5Nt.gif)

## Look at the immense freedom and power enabled by wearing the suit:

![](http://i.imgur.com/lK25Aii.gif)

# Reality

------------

![](http://i.imgur.com/j8BA1Tg.gif)

