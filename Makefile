all: index.html

index.html: index.md
	pandoc index.md -o index.html -s -i -t revealjs -V revealjs-url=files/local/revealjs -V theme=black -V transition=fade -V slideNumber=true -V fragments=false -V center=true -V history=false --css files/my-revealjs.css --css files/local/syntax/pygments.css --katex=files/local/katex/ --section-divs --slide-level=2
	mkdir -p files/local
	find archives -name "*.tar.gz" -exec tar zxf {} -C files/local \;

clean:
	rm -rf index.html files/local

