---
date: 2015-05-19
title: How Haskellers are seen and see themselves
author: Chris Done
---

# How Haskellers are seen

## The type system and separated IO is an awkward, restricting space suit:

<p><img src="http://i.imgur.com/LKENM6i.gif"></p>

## Spending most of their time gazing longingly at the next abstraction to yoink from mathematics:

<p><img src="http://i.imgur.com/RkwBNIP.gif"></p>

## Looking at anything outside the Haskell language and the type system:

<p><img src="http://i.imgur.com/7kdfsf2.gif"></p>

## Using `unsafePerformIO`:

<p><img src="http://i.imgur.com/HL5eXMj.gif"></p>

# How Haskellers see themselves

## No, it's not a space suit. It's Iron Man's suit!

<p><img src="http://i.imgur.com/AawmZmT.gif"></p>

## The suit enables him to do impressive feats with confidence and safety:

<p><img src="http://i.imgur.com/JU0o5Nt.gif"></p>

## Look at the immense freedom and power enabled by wearing the suit:

<p><img src="http://i.imgur.com/lK25Aii.gif"></p>

# Reality

------------

<p><img src="http://i.imgur.com/j8BA1Tg.gif"></p>

