with import <nixpkgs> {};

stdenv.mkDerivation {
  name = "haskellers";
  src = ./.;
  buildInputs = [
    gnumake
    pandoc
    texlive.combined.scheme-small
  ];
  installPhase = ''
    mkdir -p $out
    cp -R files index.html $out
  '';
}

